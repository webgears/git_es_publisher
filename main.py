#!/env/bin/python

import yaml, sys, os, csv, logging
import greenstalk, git, shutil

from threading     import Thread
from elasticsearch import Elasticsearch


######### Global Privates #########
def _parse_git_url(url):
  parsedUrl = url.split('/')

  '''
  parsedUrl = [
    'https:',
    '',
    'bitbucket.org',
    'webgears',
    'conan-beast'
  ]
  '''

  return parsedUrl


def _normalize_string(string):
  res = string.replace(' ', '_')
  res = res.replace('-', '_')
  return res

def _split_enum(string):
  try:
    return string.split(';')
  except:
    return string

######### Global Privates #########




class Config:

  def __init__(self, conf_file="config.yml"):
    self.confFile = conf_file
    self._config = None

    with open(self.confFile) as configDescriptor:
      self._config = yaml.safe_load(configDescriptor)

    try:
      if self._config['log']['loglevel'] == 'DEBUG':
        sys.tracebacklimit = 10
    except KeyError:
      pass


  def __repr__(self):
    return self._config


  def __str__(self):
    return self._config


  def __getitem__(self, item):
    try:
      return self._config[item]
    except KeyError:
      return None




class Logger:

  def __init__(self):
    self.config = Config()

    self.name = None

    self._logLevel = "INFO"
    self._logDir = './logs'
    self._logHandlers = []
    self.logger = ''


  def getLogger(self, name):

    self.name = name

    if self.config['log_directory']:
      self._logDir = self.config['log_directory']

    if self.config['log_level']:
      self._logLevel = self.config['log_level']

    if self.config['log_stdout']:
      self._logHandlers.append(logging.StreamHandler())

    self._logHandlers.append( logging.FileHandler(self._logDir + '/' + self.name + '.log') )

    # noinspection PyArgumentList
    logging.basicConfig(format='[%(asctime)s] [%(name)s] [%(levelname)s]: %(message)s', handlers = self._logHandlers)

    self.logger = logging.getLogger(self.name)

    self.logger.setLevel(self._logLevel)

    self.logger.debug('Logger initialized')

    return self.logger




class Application:

  def __init__(self):
    self.name = 'main'
    self.config = Config()
    self.logger = Logger().getLogger(self.name)

    self.cloners = []
    self.parsers = []

    self.logger.debug("Application init.")


  def run(self):
    clonersNum = self.config['threads']['cloner']

    if clonersNum > 0:
      self.logger.debug("Starting " + str(clonersNum) + " cloners")

      if self.config['reread_input']:
        self.logger.info("Loading git repos")
        loaded = self.enqueue_bitbucket_repos()
        self.logger.info("Loaded %d repos", loaded)

      for pid in range(clonersNum):
        cloner = RepoCloner(pid)
        cloner.start()
        self.cloners.append(cloner)

    parsersNum = self.config['threads']['parser']

    if parsersNum > 0:
      self.logger.debug("Starting %d parsers", parsersNum)
      self.logger.debug("Adding ES index with mapping")

      self.put_es_mapping()

      for pid in range(parsersNum):
        parser = RepoParser(pid)
        parser.start()
        self.parsers.append(parser)

    if len(self.parsers) > 0:
      for parser in self.parsers:
        parser.join()

    if len(self.cloners) > 0:
      for cloner in self.cloners:
        cloner.join()


  def enqueue_bitbucket_repos(self):

    q_host = self.config['queue']['beanstalk']['host']
    q_port = self.config['queue']['beanstalk']['port']

    queue  = greenstalk.Client(( q_host, q_port), use='clone', watch='clone')

    count  = 0

    with open(self.config['links_input']) as input_source:
      for repo in input_source:
        queue.put(repo, ttr=6000)
        count += 1
      input_source.close()

    return count


  def put_es_mapping(self):
    es_host     = self.config['es']['address']
    es_port     = self.config['es']['port']
    es_index    = self.config['es']['index']
    es_shards   = self.config['es']['shards']
    es_replicas = self.config['es']['replicas']

    es          = Elasticsearch([{'host': es_host, 'port': es_port}])

    es_mapping  = {
      "settings": {
        "number_of_shards": es_shards,
        "number_of_replicas": es_replicas
      },
      "mappings": {
        "properties":{
          "@dtime": {
            "type": "date",
            "format": "epoch_second"
          },
          "hash": {
            "type": "text"
          },
          "message": {
            "type": "text"
          },
          "author": {
            "type": "text",
            "fielddata": True
          },
          "repo": {
            "type": "text",
            "fielddata": True
          },
          "component": {
            "type": "text",
            "fielddata": True
          },
          "project": {
            "type": "text",
            "fielddata": True
          },
          "isdemo": {
            "type": "boolean"
          }
        }
      }
    }

    if es.indices.exists(es_index):
      try:
        es.indices.delete(index=es_index)
        es.indices.create(index=es_index, body=es_mapping)
      except Exception as es1:
        self.logger.error(es1)
    else:
      es.indices.create(index=es_index, body=es_mapping)




class RepoCloner(Thread):
  def __init__(self, pid:int):
    Thread.__init__(self)

    self.name   = 'RepoCloner-' + str(pid)
    self.daemon = True

    self.config = Config()
    self.logger = Logger().getLogger(self.name)

    q_host      = self.config['queue']['beanstalk']['host']
    q_port      = self.config['queue']['beanstalk']['port']

    self.queue      = greenstalk.Client((q_host, q_port), use='clone', watch='clone')
    self.next_queue = greenstalk.Client((q_host, q_port), use='parse', watch='parse')


  def run(self):
    self.logger.info("%s starting", self.name)

    while 1:
      self.logger.debug("%s main loop", self.name)

      self.logger.debug("Awaiting jobs")

      job = self.queue.reserve()
      url = job.body.rstrip('\n')

      self.logger.debug("Got job data: %s", url)

      cloned = self._clone_git_if_not_cloned(url)

      if cloned:
        self.next_queue.put(url, ttr=6000)
        self.queue.delete(job)


  def _clone_git_if_not_cloned(self, url):
    parsedUrl = _parse_git_url(url)

    reposWorkdir = self.config['repos_workdir']

    targetDir = reposWorkdir + '/' + parsedUrl[4]

    gitrepo = "git@" + parsedUrl[2] + ":" + parsedUrl[3] + '/' + parsedUrl[4]

    try:
      self.logger.info("Cloning %s", gitrepo)
      git.Git(reposWorkdir).clone(gitrepo)
      self.logger.debug("Cloning %s done", gitrepo)
      return True

    except git.exc.GitCommandError:
      if os.path.isdir(targetDir):
        self.logger.debug("%s directory exists", targetDir)

        try:
          repo = git.Repo(targetDir)
          repo.git_dir
          self.logger.debug("%s directory is a valid git repo", targetDir)

          if self.config['pull_new_commits']:
            self.logger.debug("Updating repo %s", targetDir)
            try:
              repo.remotes.origin.pull()
            except git.exc.GitCommandError:
              self.logger.warning("Cannot pull repository %s", targetDir)

          return True

        except git.exc.InvalidGitRepositoryError:
          self.logger.warning('Cloning error')
          self.logger.debug('Re-cloning %s', gitrepo)
          shutil.rmtree(targetDir)
          status = self._clone_git_if_not_cloned(url)
          return status

      else:
        self.logger.error("WTF")



class RepoParser(Thread):
  def __init__(self, pid:int):
    Thread.__init__(self)

    self.name   = 'RepoParser-' + str(pid)
    self.daemon = True


    self.config = Config()
    self.logger = Logger().getLogger(self.name)

    q_host      = self.config['queue']['beanstalk']['host']
    q_port      = self.config['queue']['beanstalk']['port']

    self.queue      = greenstalk.Client((q_host, q_port), use='parse', watch='parse')
    self.prev_queue = greenstalk.Client((q_host, q_port), use='clone', watch='clone')

    es_host     = self.config['es']['address']
    es_port     = self.config['es']['port']

    self.index  = self.config['es']['index']
    self.es     = Elasticsearch([{'host': es_host, 'port': es_port}])

    self.mapping = {}
    self.logger.debug("Reading mappings for authors")
    self.mapping['author'] = self.read_author_mapping()
    self.logger.debug("Reading mappings for repos")
    self.mapping['repos']  = self.read_repo_mappings()


  def read_author_mapping(self):
    authorMapping = {}
    with open(self.config['author_mapping']) as csvfile:
      csvdata = csv.reader(csvfile, delimiter=',')
      for row in csvdata:
        (nickName, email, name) = row

        if name == 'external':
          name = 'unknown'
        if name == '?':
          name = 'unknown'

        joiner = '_'
        name = joiner.join(name.split(' '))
        authorMapping[nickName] = name

    return authorMapping


  def read_repo_mappings(self):
    componentMapping = {}
    projectMapping   = {}
    repoMapping      = {}

    with open(self.config['repo_component_mapping']) as csvfile1:
      repodata = csv.reader(csvfile1, delimiter=',')
      for row in repodata:
        (url, component, project, demo, comment) = row
        repoName = _parse_git_url(url)[4]

        repoMapping[repoName] = {}
        repoMapping[repoName]['components'] = []
        repoMapping[repoName]['projects'] = []

        if component:
          for item in component.split(';'):
            item = _normalize_string(item.strip())
            try:
              componentMapping[item].append(repoName)
            except KeyError:
              componentMapping[item] = []
              componentMapping[item].append(repoName)
            repoMapping[repoName]['components'].append(item)

        if project:
          for item in project.split(';'):
            item = _normalize_string(item.strip())
            try:
              projectMapping[item].append(repoName)
            except KeyError:
              projectMapping[item] = []
              projectMapping[item].append(repoName)

            repoMapping[repoName]['projects'].append(item)

      with open(self.config['project_component_mapping']) as csvfile2:
        projectdata = csv.reader(csvfile2, delimiter=',')
        for row in projectdata:
          (project, projectFull, components, comment) = row

          for component in components.split(';'):
            component = _normalize_string(component.strip())
            if component:
              for repoName in componentMapping[component]:
                project = _normalize_string(project.strip())
                try:
                  projectMapping[project].append(repoName)
                except KeyError:
                  projectMapping[project] = []
                  projectMapping[project].append(repoName)

                try:
                  repoMapping[repoName]['projects'].append(project)
                except KeyError:
                  repoMapping[repoName]['projects'] = []
                  repoMapping[repoName]['projects'].append(project)

    return repoMapping





  def run(self):
    self.logger.info("%s starting", self.name)

    while 1 :
      self.logger.debug("%s main loop", self.name)

      self.logger.debug("Awaiting jobs")

      job = self.queue.reserve()
      url = job.body.rstrip('\n')

      self.logger.debug("Got job data: %s", url)

      self.logger.debug("Trying to open: %s", url)

      parsedUrl = _parse_git_url(url)

      targetDir = self.config['repos_workdir'] + '/' + parsedUrl[4]

      commitList = []

      try:
        git.Repo(targetDir).git_dir
        self.logger.debug("%s directory is a valid git repo", targetDir)
      except (git.exc.InvalidGitRepositoryError, git.exc.NoSuchPathError):
        self.logger.warning("%s directory is NOT a valid git repo", targetDir)
        self.prev_queue.put(url)
        self.queue.delete(job)
      finally:
        repo =  git.Repo(targetDir)
        try:
          commitList = list(repo.iter_commits())
        except:
          self.logger.warning("Master branch not found in repo %s", targetDir)
        finally:
          self.logger.debug("Found %d commits in %s repo", len(commitList), targetDir)

          bulkData = []

          for commit in commitList:
            item = {}
            repo = parsedUrl[4]
            item['repo']    = _normalize_string(repo)
            item['hash']    = commit.hexsha
            item['@dtime']  = commit.committed_date

            try:
              item['author'] = self.mapping['author'][commit.author.name]
            except KeyError:
              item['author'] = 'unknown'


            if item['author'] == 'unknown':
              self.logger.warning("Couldnt find author mapping for commit %s in repo %s", item['hash'], repo)


            item['project'] = []
            try:
              item['project'] = self.mapping['repos'][repo]['projects']
            except KeyError:
              self.logger.warning("No mapping project found for repo %s", repo)
            if len(item['project']) == 0:
              item['project'] = 'None'

            item['component'] = []
            try:
              item['component'] = self.mapping['repos'][repo]['components']
            except KeyError:
              self.logger.warning("No component mapping found for repo %s", repo)
            if len(item['component']) == 0:
              item['component'] = 'None'

            item['message'] = commit.message

            bulkData.append(item)


          for item in bulkData:
            try:
              self.es.index(self.config['es']['index'], body=item)
            except Exception as e:
              self.logger.warning("Cannot publish commit to ES")
              self.logger.error(e)

          self.logger.info("Finished parsing %s", targetDir)

          self.queue.delete(job)



if __name__ == '__main__':
  Application().run()